package main

import (
    // "fmt"
    "math"
    "log"
    "encoding/json"
    "net/http"
    "strconv"
    "database/sql"
	_ "github.com/lib/pq"
)

/* representation of a geolocation point */
type Point struct {
    Name      string    `json:"name"`
    Lat	 	  float64    `json:"lat"`
    Long      float64    `json:"long"`
}

/* representation of a list of geoloocation points */
type Points []Point

/* method to find nearby points */
/* requires a geo location point, and radial distance */
/* returns list of all points falling in given range */
func getNearbyPoints(w http.ResponseWriter, r *http.Request) {

	// checking for authentication here, should be a decorator rather
	accessToken := r.Header.Get("Authorization")
	if (accessToken != "some-very-strong-hashing-method-generated-token"){
		// creating JSON response
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusForbidden)
	}

	pointLat, err := strconv.ParseFloat(r.URL.Query().Get("lat"), 64)
	pointLong, err := strconv.ParseFloat(r.URL.Query().Get("long"), 64)
	radius, err  := strconv.ParseFloat(r.URL.Query().Get("radius"), 64)

    var (
		name string
		lat float64
		long float64
	)

    // creating db connection here, I would like to keep authentication secrets in a separate file as constants 
	db, err := sql.Open("postgres", "user=tunnelsup password=Amazingme1* dbname=redcarpet sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	// query to fetch all points from database
	rows, err := db.Query("SELECT name, lat, long FROM geolocations")

	// logic which finds out the nearby points
	defer rows.Close()
	points := Points{}
	for rows.Next() {
		err := rows.Scan(&name, &lat, &long)
		if err != nil {
			log.Fatal(err)
		}
		point := Point{Name: name, Lat: lat, Long: long}
		distance := Distance(pointLat, pointLong, lat, long)

		if distance <= radius {
			points = append(points, point)
		}
	}

	if len(points) > 0 {
			log.Println("Points in range are", points)
		} else {
			log.Println("No points fall in range")	
		}

	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	// creating JSON response
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(points)
}

/* method to create new point in database */
/* requires a geo location point and its name */
/* basic checks for input validations are skipped here, but there should be basic validations before inserting into database */
func createPoint(w http.ResponseWriter, r *http.Request) {

	// checking for authentication here, should be a decorator rather
	accessToken := r.Header.Get("Authorization")
	if (accessToken != "some-very-strong-hashing-method-generated-token"){
		// creating JSON response
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusForbidden)
	}

	// parsing POST request params
	r.ParseForm()
	pointLat, err := strconv.ParseFloat(r.Form.Get("lat"), 64)
	if err != nil {
		log.Fatal(err)
	}
	pointLong, err := strconv.ParseFloat(r.Form.Get("long"), 64)
	if err != nil {
		log.Fatal(err)
	}
	name := r.Form.Get("name")
	log.Println(err)

	// creating db connection here, I would like to keep authentication secrets in a separate file as constants 
	db, err := sql.Open("postgres", "user=tunnelsup password=Amazingme1* dbname=redcarpet sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	// db query to insert data into database
	rows, err := db.Query("INSERT INTO geolocations (name, lat, long) VALUES ($1, $2, $3)", name, pointLat, pointLong)
	if err != nil {
		log.Fatal(err, rows)
	}

	// creating JSON response
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(Point{Name: name, Lat: pointLat, Long: pointLong})

}

// logic to calculate distance between two geo locations
func hsin(theta float64) float64 {
	return math.Pow(math.Sin(theta/2), 2)
}

func Distance(lat1, lon1, lat2, lon2 float64) float64 {
    // convert to radians
    // must cast radius as float to multiply later
	var la1, lo1, la2, lo2, r float64
	la1 = lat1 * math.Pi / 180
	lo1 = lon1 * math.Pi / 180
	la2 = lat2 * math.Pi / 180
	lo2 = lon2 * math.Pi / 180

	r = 6378100 // Earth radius in METERS

	// calculate
	h := hsin(la2-la1) + math.Cos(la1)*math.Cos(la2)*hsin(lo2-lo1)

	return 2 * r * math.Asin(math.Sqrt(h))
}

func checkUsingPostgres(w http.ResponseWriter, r *http.Request) {

	pointLat, err := strconv.ParseFloat(r.URL.Query().Get("lat"), 64)
	pointLong, err := strconv.ParseFloat(r.URL.Query().Get("long"), 64)
	radius, err  := strconv.ParseFloat(r.URL.Query().Get("radius"), 64)

	var (
		name string
		lat float64
		long float64
	)

	// creating db connection here, I would like to keep authentication secrets in a separate file as constants 
	db, err := sql.Open("postgres", "user=tunnelsup password=Amazingme1* dbname=redcarpet sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	rows, err := db.Query("SELECT name, lat, long FROM geolocations where earth_box(ll_to_earth($1, $2), $3) @> ll_to_earth(geolocations.lat, geolocations.lng)", pointLat, pointLong, radius)
	if err != nil {
		log.Fatal(err)
	}

	// logic which finds out the nearby points
	defer rows.Close()
	points := Points{}
	for rows.Next() {
		err := rows.Scan(&name, &lat, &long)
		if err != nil {
			log.Fatal(err)
		}
		point := Point{Name: name, Lat: lat, Long: long}
		distance := Distance(pointLat, pointLong, lat, long)

		if distance <= radius {
			points = append(points, point)
		}
	}

	if len(points) > 0 {
			log.Println("Points in range are", points)
		} else {
			log.Println("No points fall in range")	
		}

	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	// creating JSON response
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(points)
}

// implementing user authentication
func authenticateUser(w http.ResponseWriter, r *http.Request) {

	// parsing POST request params
	r.ParseForm()
	username := r.Form.Get("username")
	password := r.Form.Get("password")

	var response map[string]string
	response = make(map[string]string)
	if username == "username" && password == "password" {
		// creating JSON response
		response["accessToken"] = "some-very-strong-hashing-method-generated-token"
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(response)
	} else {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
	}
}